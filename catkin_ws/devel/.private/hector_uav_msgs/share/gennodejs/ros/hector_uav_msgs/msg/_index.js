
"use strict";

let MotorPWM = require('./MotorPWM.js');
let RC = require('./RC.js');
let VelocityZCommand = require('./VelocityZCommand.js');
let MotorCommand = require('./MotorCommand.js');
let ThrustCommand = require('./ThrustCommand.js');
let MotorStatus = require('./MotorStatus.js');
let Supply = require('./Supply.js');
let HeightCommand = require('./HeightCommand.js');
let RawImu = require('./RawImu.js');
let Altimeter = require('./Altimeter.js');
let Compass = require('./Compass.js');
let PositionXYCommand = require('./PositionXYCommand.js');
let RawRC = require('./RawRC.js');
let YawrateCommand = require('./YawrateCommand.js');
let RawMagnetic = require('./RawMagnetic.js');
let ServoCommand = require('./ServoCommand.js');
let HeadingCommand = require('./HeadingCommand.js');
let ControllerState = require('./ControllerState.js');
let AttitudeCommand = require('./AttitudeCommand.js');
let RuddersCommand = require('./RuddersCommand.js');
let VelocityXYCommand = require('./VelocityXYCommand.js');
let TakeoffActionFeedback = require('./TakeoffActionFeedback.js');
let TakeoffGoal = require('./TakeoffGoal.js');
let TakeoffActionGoal = require('./TakeoffActionGoal.js');
let TakeoffResult = require('./TakeoffResult.js');
let LandingFeedback = require('./LandingFeedback.js');
let PoseActionResult = require('./PoseActionResult.js');
let LandingActionGoal = require('./LandingActionGoal.js');
let LandingGoal = require('./LandingGoal.js');
let TakeoffFeedback = require('./TakeoffFeedback.js');
let PoseAction = require('./PoseAction.js');
let PoseResult = require('./PoseResult.js');
let PoseFeedback = require('./PoseFeedback.js');
let PoseActionFeedback = require('./PoseActionFeedback.js');
let PoseActionGoal = require('./PoseActionGoal.js');
let LandingActionResult = require('./LandingActionResult.js');
let PoseGoal = require('./PoseGoal.js');
let LandingAction = require('./LandingAction.js');
let LandingActionFeedback = require('./LandingActionFeedback.js');
let LandingResult = require('./LandingResult.js');
let TakeoffAction = require('./TakeoffAction.js');
let TakeoffActionResult = require('./TakeoffActionResult.js');

module.exports = {
  MotorPWM: MotorPWM,
  RC: RC,
  VelocityZCommand: VelocityZCommand,
  MotorCommand: MotorCommand,
  ThrustCommand: ThrustCommand,
  MotorStatus: MotorStatus,
  Supply: Supply,
  HeightCommand: HeightCommand,
  RawImu: RawImu,
  Altimeter: Altimeter,
  Compass: Compass,
  PositionXYCommand: PositionXYCommand,
  RawRC: RawRC,
  YawrateCommand: YawrateCommand,
  RawMagnetic: RawMagnetic,
  ServoCommand: ServoCommand,
  HeadingCommand: HeadingCommand,
  ControllerState: ControllerState,
  AttitudeCommand: AttitudeCommand,
  RuddersCommand: RuddersCommand,
  VelocityXYCommand: VelocityXYCommand,
  TakeoffActionFeedback: TakeoffActionFeedback,
  TakeoffGoal: TakeoffGoal,
  TakeoffActionGoal: TakeoffActionGoal,
  TakeoffResult: TakeoffResult,
  LandingFeedback: LandingFeedback,
  PoseActionResult: PoseActionResult,
  LandingActionGoal: LandingActionGoal,
  LandingGoal: LandingGoal,
  TakeoffFeedback: TakeoffFeedback,
  PoseAction: PoseAction,
  PoseResult: PoseResult,
  PoseFeedback: PoseFeedback,
  PoseActionFeedback: PoseActionFeedback,
  PoseActionGoal: PoseActionGoal,
  LandingActionResult: LandingActionResult,
  PoseGoal: PoseGoal,
  LandingAction: LandingAction,
  LandingActionFeedback: LandingActionFeedback,
  LandingResult: LandingResult,
  TakeoffAction: TakeoffAction,
  TakeoffActionResult: TakeoffActionResult,
};
