# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/filter.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/filter.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/filter/ekf.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/filter/ekf.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/global_reference.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/global_reference.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurement.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurement.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/baro.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/baro.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/gps.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/gps.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/gravity.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/gravity.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/heading.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/heading.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/height.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/height.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/magnetic.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/magnetic.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/poseupdate.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/poseupdate.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/rate.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/rate.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/measurements/zerorate.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/measurements/zerorate.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/parameters.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/parameters.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/pose_estimation.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/pose_estimation.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/state.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/state.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/system.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/system.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/system/generic_quaternion_system_model.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/system/generic_quaternion_system_model.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/system/ground_vehicle_model.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/system/ground_vehicle_model.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/system/imu_model.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/system/imu_model.cpp.o"
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/src/types.cpp" "/catkin_ws/build/hector_pose_estimation_core/CMakeFiles/hector_pose_estimation.dir/src/types.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"hector_pose_estimation_core\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/catkin_ws/src/hector_quadrotor/hector_pose_estimation_core/include"
  "/catkin_ws/devel/.private/hector_pose_estimation_core/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
